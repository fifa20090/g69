//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L4H" //  Level name
        
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        diagonals()// строит диагонали
        
        
        
        
    }
    func engle(){
        
        while frontIsClear{
            move()
        }
    }
    
    
    func turnAround(){
        turnRight()
        turnRight()
    }
    func tripleRight(){
        turnRight()
        turnRight()
        turnRight()
    }
    
    
    
    func build24(){
        
        while leftIsClear{
            put()
            move()
            tripleRight()
            move()
            
            turnRight()
            
        }
        put()
    }
    
    
    func diagonals(){
        
        build13()//1 диагональ
        turnAround()
        
        engle()// идем к углу
        turnAround()
        build24()//вторая диагональ
        
        
        
    }
    
    
    
}
